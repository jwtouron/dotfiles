;;;; General Settings

(setf *input-window-gravity* :center
      *message-window-gravity* :center
      *mouse-focus-policy* :sloppy
      *window-border-style* :thin
      *window-format* "%m%n%s%50t - %c")
(set-font "-*-dejavu sans mono-medium-r-normal-*-20-*-*-*-*-*-*-*")
