;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

; colors
include-file = ~/.config/polybar/black-metal-immortal-theme

[bar/i3]
monitor = ${env:MONITOR}

height = 30px

background = ${colors.background-darker}

line-size = 5

padding = 1

modules-left = i3
modules-right = pulseaudio sep cpu memory updates sep weather sep date sep powermenu-herbstluftwm

tray-position = right
tray-background = ${colors.background-lighter}

font-0 = MesloLGSDZ Nerd Font:style=Regular:size=10;2

[bar/herbstluftwm]
monitor = ${env:MONITOR}

height = 30px

background = ${colors.background-darker}

line-size = 5

padding = 1

modules-left = ewmh
modules-right = pulseaudio sep cpu memory updates sep weather sep date

tray-position = right
tray-background = ${colors.background-lighter}

font-0 = Hack Nerd Font:style=Regular:size=10;2
font-1 = Caskaydia Cove Nerd Font:style=Regular:size=10;2
font-2 = MesloLGSDZ Nerd Font:style=Regular:size=10;2

[bar/stumpwm]
monitor = ${env:MONITOR}

override-redirect = false
enable-ipc = true

height = 30px

background = ${colors.background-darker}

line-size = 5

padding = 1

modules-left = ewmh-stumpwm
modules-right = pulseaudio sep cpu void void memory void void updates sep weather sep date sep powermenu-stumpwm

tray-position = right
tray-background = ${colors.background-lighter}

font-0 = MesloLGSDZ Nerd Font:style=Regular:size=10;2
font-1 = FiraCode Nerd Font:style=Regular:size=10;2
font-2 = DejaVuSansMono Nerd Font:style=Book:size=10;2

[module/left]
type = custom/text
content-foreground = ${colors.background-lighter}
content-background = ${colors.background-darker}
content-overline = ${colors.background-darker}
content-underline = ${colors.background-darker}
content = ""

[module/right]
type = custom/text
content-foreground = ${colors.background-lighter}
content-background = ${colors.background-darker}
content-overline = ${colors.background-darker}
content-underline = ${colors.background-darker}
content = ""

[module/sep]
type = custom/text
content = " "

[module/void]
type = custom/text
content = " "
content-background = ${colors.background-lighter}
content-overline = ${colors.background-darker}
content-underline = ${colors.background-darker}

[module/i3]
type = internal/i3

pin-workspaces = true
show-urgent = true

format = <label-state><label-mode>
format-foreground = ${colors.background-lighter}
format-background = ${colors.background-darker}
format-underline = ${colors.background-darker}
format-overline = ${colors.background-darker}

label-mode-padding = 1
label-mode-foreground = ${colors.workspace-occupied-fg}
label-mode-background = ${colors.background-lighter}

label-focused = %name%
label-focused-foreground = ${colors.workspace-active-fg}
label-focused-background = ${colors.background-lighter}
label-focused-padding = 1

label-unfocused = %name%
label-unfocused-background = ${colors.background-lighter}
label-unfocused-foreground = ${colors.workspace-occupied-fg}
label-unfocused-padding = 1

label-visible = %name%
label-visible-background = ${colors.background-lighter}
label-visible-foreground = ${colors.workspace-occupied-fg}
label-visible-padding = 1

label-urgent = %name%
label-urgent-foreground = ${colors.urgent}
label-urgent-background = ${colors.background-lighter}
label-urgent-underline = ${colors.urgent}
label-urgent-overline = ${colors.urgent}
label-urgent-padding = 1

[module/ewmh]
type = internal/xworkspaces

reverse-scroll = true

label-active = %name%
label-active-foreground = ${colors.workspace-active-fg}
label-active-background = ${colors.workspace-active-bg}
label-active-underline = ${colors.background-darker}
label-active-overline = ${colors.background-darker}
label-active-padding = 1

label-occupied = %name%
label-occupied-foreground = ${colors.workspace-occupied-fg}
label-occupied-background = ${colors.background-lighter}
label-occupied-underline = ${colors.background-darker}
label-occupied-overline = ${colors.background-darker}
label-occupied-padding = 1

label-urgent = %name%
label-urgent-foreground = ${colors.urgent}
label-urgent-background = ${colors.background-lighter}
label-urgent-underline = ${colors.urgent}
label-urgent-overline = ${colors.urgent}
label-urgent-padding = 1

label-empty = %name%
label-empty-foreground = ${colors.workspace-empty-fg}
label-empty-background = ${colors.background-lighter}
label-empty-underline = ${colors.background-darker}
label-empty-overline = ${colors.background-darker}
label-empty-padding = 1

[module/ewmh-stumpwm]
type = internal/xworkspaces

reverse-scroll = true

label-active = %index% %name%
label-active-foreground = ${colors.workspace-active-fg}
label-active-background = ${colors.workspace-active-bg}
label-active-underline = ${colors.background-darker}
label-active-overline = ${colors.background-darker}
label-active-padding = 1

label-occupied = %index% %name%
label-occupied-foreground = ${colors.workspace-occupied-fg}
label-occupied-background = ${colors.background-lighter}
label-occupied-underline = ${colors.background-darker}
label-occupied-overline = ${colors.background-darker}
label-occupied-padding = 1

label-urgent = %index% %name%
label-urgent-foreground = ${colors.urgent}
label-urgent-background = ${colors.background-lighter}
label-urgent-underline = ${colors.urgent}
label-urgent-overline = ${colors.urgent}
label-urgent-padding = 1

label-empty = %index% %name%
label-empty-foreground = ${colors.workspace-empty-fg}
label-empty-background = ${colors.background-lighter}
label-empty-underline = ${colors.background-darker}
label-empty-overline = ${colors.background-darker}
label-empty-padding = 1

[module/pulseaudio]
type = internal/pulseaudio

format-volume = <ramp-volume> <label-volume>
label-volume = "%percentage%% "

label-muted = "  0% "

ramp-volume-0 = " "
ramp-volume-1 = " "
ramp-volume-2 = " "

click-right = pavucontrol

format-volume-foreground = ${colors.pulseaudio}
format-volume-background = ${colors.background-lighter}
format-volume-underline = ${colors.background-darker}
format-volume-overline = ${colors.background-darker}

format-muted-foreground = #f55
format-muted-background = ${colors.background-lighter}
format-muted-underline = ${colors.background-darker}
format-muted-overline = ${colors.background-darker}

[module/cpu]
type = custom/script
exec = ~/.config/polybar/cpu.sh
interval = 1
click-left = xterm -class xterm-floating -geometry 120x36 -e htop

format-foreground = ${colors.cpu}
format-background = ${colors.background-lighter}
format-underline = ${colors.background-darker}
format-overline = ${colors.background-darker}

[module/memory]
type = custom/script
exec = ~/.config/polybar/memory.sh
interval = 1
click-left = xterm -class xterm-floating -geometry 120x36 -e htop

format-foreground = ${colors.memory}
format-background = ${colors.background-lighter}
format-underline = ${colors.background-darker}
format-overline = ${colors.background-darker}

[module/updates]
type = custom/script
exec = ~/.config/polybar/updates.sh
interval = 21600  # 6 hours
click-left = ~/.config/polybar/updates.sh click-left
click-right = ~/.config/polybar/updates.sh click-right
exec-if = command -v checkupdates || command -v apt || command -v dnf

format-background = ${colors.background-lighter}
format-foreground = ${colors.updates}
format-underline = ${colors.background-darker}
format-overline = ${colors.background-darker}

[module/weather]
type = custom/script
exec = ~/.local/bin/weather-simple.sh
interval = 1800
click-left = xterm -class xterm-floating -geometry 80x40 -e ~/.local/bin/weather-detailed.sh

format-foreground = ${colors.weather}
format-background = ${colors.background-lighter}
format-underline = ${colors.background-darker}
format-overline = ${colors.background-darker}

[module/date]
type = custom/script
exec = date +" %a, %d %b, %H:%M "
click-left = yad --calendar --undecorated --no-buttons --mouse --close-on-unfocus

format-foreground = ${colors.date}
format-background = ${colors.background-lighter}
format-overline = ${colors.background-darker}
format-underline = ${colors.background-darker}

[module/powermenu-i3]
type = custom/text
content = ""
click-left = ~/.config/i3/powermenu.sh

content-padding = 1
content-background = ${colors.background-lighter}
content-foreground = #f55
content-underline = ${colors.background-darker}
content-overline = ${colors.background-darker}

[module/powermenu-herbstluftwm]
type = custom/text
content = ""
click-left = ~/.config/herbstluftwm/powermenu.sh

content-padding = 1
content-background = ${colors.background-lighter}
content-foreground = #f55
content-underline = ${colors.background-darker}
content-overline = ${colors.background-darker}

[module/powermenu-stumpwm]
type = custom/text
content = ""
click-left = ~/.config/stumpwm/powermenu.sh

content-padding = 1
content-background = ${colors.background-lighter}
content-foreground = #f55
content-underline = ${colors.background-darker}
content-overline = ${colors.background-darker}

; vim:ft=dosini
